provider "aws" {
    region = "ap-south-1"
}
data "aws_iam_policy_document" "umesh-role" {
    statement {
        effect = "Allow"

        principals {
          type        = "Service"
          identifiers = ["eks.amozonaws.com"]         
        }

        actions = ["sts:AssumeRole"]
    }
}

resource "aws_iam_role" "uk-role" {
  name               = "eks_cluster-1"
  assume_role_policy = data.aws_iam_policy_document.umesh-role.json
}
 
resource "aws_iam_role_policy_attachment" "AmazonEKSClusterpolicy" {
    policy_arn = "arn:aws:iam:aws:policy/AmazonEkSClusterpolicy"
    role       = aws_iam_role.uk-role.name
}

resource "aws_iam_role_policy_attachment" "amezonEKSVPCResourceController" {
  policy_arn = "arn:aws:iam:aws:policy/AmazonEKSVPCResourceController"
  role       =  aws_iam_role.uk-role.name
}

resource "aws_eks_cluster" "eks-cluster" {
  name     = "my cluster"
  role_arn = aws_iam_role.uk-role.role_arn

  vpc_config {
    subnet_ids = [
      "subnet-0100bd5dc5df47ee7",
      "subnet-0b6b8335eb2cf965f",
      "subnet-0488fcec58159b184"
    ]
  }


  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSClusterpolicy,
    aws_iam_role_policy_attachment.AmazonEKSVPCResourceController,
  ]
} 
